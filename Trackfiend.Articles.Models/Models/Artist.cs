﻿namespace Trackfiend.Articles.Models.Models
{
    public class Artist
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Mbid { get; set; }
    }
}
