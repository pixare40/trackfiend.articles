﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Trackfiend.Articles.Models.Models
{
    public class Article

    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId DocumentId { get; set; }

        [BsonElement("id")]
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [BsonElement("title")]
        [JsonPropertyName("title")]
        public string Title { get; set; }

        [BsonElement("body")]
        [JsonPropertyName("body")]
        public string Body { get; set; }

        [BsonElement("url")]
        [JsonPropertyName("url")]
        public string Url { get; set; }

        [BsonElement("summary")]
        [JsonPropertyName("summary")]
        public string Summary { get; set; }

        [BsonElement("article_picture")]
        [JsonPropertyName("article_picture")]
        public string ArticlePicture { get; set; }

        [BsonElement("article_type")]
        [JsonPropertyName("article_type")]
        public string ArticleType { get; set; }

        [BsonElement("artists")]
        [JsonPropertyName("artists")]
        public List<Artist> Artists { get; set; }
    }
}
