﻿namespace Trackfiend.Articles.Models.ConfigModels
{
    public class DatabaseConfigurationModel
    {
        public const string DatabaseConfiguration = "DatabaseConfiguration";

        public string DatabaseName { get; set; }

        public string ConnectionString { get; set; }

        public string CollectionName { get; set; }
    }
}
