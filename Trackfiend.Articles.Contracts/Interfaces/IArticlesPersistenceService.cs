﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Trackfiend.Articles.Models.Models;

namespace Trackfiend.Articles.Contracts.Interfaces
{
    public interface IArticlesPersistenceService
    {
        Task<IEnumerable<Article>> GetPagedArticles(int page, int pageSize);

        Task<Article> SaveArticle(Article article);

        Task<long> GetTotalArticles();

        Article GetArticle(string articleId);

        Task<Article> CreateArticleAsync(Article article);

        Task<Article> UpdateArticleAsync(Article article);

        Task<IEnumerable<Article>> GetArticleByTypeAsync(string type, int page);

        Task<long> GetTotalArticlesByType(string type);
    }
}
