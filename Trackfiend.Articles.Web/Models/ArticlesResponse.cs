﻿using System.Collections.Generic;
using Trackfiend.Articles.Models.Models;

namespace Trackfiend.Articles.Models
{
    public class ArticlesResponse
    {
        public List<Article> Articles { get; set; }

        public long Total { get; set; }
    }
}
