﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trackfiend.Articles.Contracts.Interfaces;
using Trackfiend.Articles.Models;
using Trackfiend.Articles.Models.Models;

namespace Trackfiend.Articles.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        private readonly IArticlesPersistenceService articlesPersistenceService;

        public ArticlesController(IArticlesPersistenceService articlesPersistenceService)
        {
            this.articlesPersistenceService = articlesPersistenceService;
        }

        [HttpGet("GetArticles/{page}")]
        public async Task<ActionResult<ArticlesResponse>> GetArticles(int page)
        {
            IEnumerable<Article> articles = await articlesPersistenceService.GetPagedArticles(page, 10);
            long total = await articlesPersistenceService.GetTotalArticles();

            var articlesResult = new ArticlesResponse { Articles = articles.ToList(), Total = total };

            return Ok(articlesResult);
        }

        [HttpPost("SaveArticle")]
        public async Task<ActionResult<Article>> SaveArticle(Article article)
        {
            // [TODO] Add validation

            var result = await articlesPersistenceService.SaveArticle(article);
            return Ok(result);
        }

        [HttpGet("GetArticle/{articleId}")]
        public ActionResult<Article> GetArticle(string articleId)
        {
            // [TODO] validate id

            Article article = articlesPersistenceService.GetArticle(articleId);

            return Ok(article);
        }

        [HttpPost("UpdateArticle")]
        public async Task<ActionResult<Article>> UpdateArticle(Article article)
        {
            await articlesPersistenceService.UpdateArticleAsync(article);

            return Ok(article); 
        }

        [HttpPost("CreateArticle")]
        public async Task<ActionResult<Article>> CreateArticle(Article article)
        {
            await articlesPersistenceService.CreateArticleAsync(article);

            return Ok(article);
        }

        [HttpGet("GetArticlesByType/{type}/{page:int}")]
        public async Task<ActionResult<ArticlesResponse>> GetArticlesByTypeAsync(string type, int page)
        {
            IEnumerable<Article> articles = await articlesPersistenceService.GetArticleByTypeAsync(type, page);
            var total = await articlesPersistenceService.GetTotalArticlesByType(type);

            return Ok(new ArticlesResponse { Total = total, Articles= articles.ToList() });
        }
    }
}
