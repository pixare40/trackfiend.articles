﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Trackfiend.Articles.Contracts.Interfaces;
using Trackfiend.Articles.Models.ConfigModels;
using Trackfiend.Articles.Models.Models;

namespace Trackfiend.Articles.Persistence.Services
{
    public class ArticlesPersistenceService : IArticlesPersistenceService
    {
        private readonly IMongoCollection<Article> articlesCollection;

        public ArticlesPersistenceService(IOptions<DatabaseConfigurationModel> dbConfiguration)
        {
            DatabaseConfigurationModel dbModel = dbConfiguration.Value;

            MongoClient client = new MongoClient(dbModel.ConnectionString);
            IMongoDatabase database = client.GetDatabase(dbModel.DatabaseName);

            articlesCollection = database.GetCollection<Article>(dbModel.CollectionName);
        }

        public async Task<Article> CreateArticleAsync(Article article)
        {
            article.Id = Guid.NewGuid().ToString();
            await articlesCollection.InsertOneAsync(article);

            return article;
        }

        public Article GetArticle(string articleId)
        {
            var articleFilter = Builders<Article>.Filter.Eq(x => x.Id, articleId);
            Task<Article> result = articlesCollection.Find(articleFilter).SingleAsync();

            return result.Result;
        }

        public async Task<IEnumerable<Article>> GetArticleByTypeAsync(string type, int page)
        {
            var articleFilter = Builders<Article>.Filter.Eq(x => x.ArticleType, type);
            IAsyncCursor<Article> result = await articlesCollection.FindAsync(articleFilter);
            return result.ToList().Skip((page - 1) * 10)
                .Take(10);
        }

        public async Task<IEnumerable<Article>> GetPagedArticles(int page, int pageSize)
        {
            page = page == 0 ? 1 : page;

            var articlesFilter = Builders<Article>.Filter.Empty;
            List<Article> data = await articlesCollection.Find(articlesFilter)
                .Skip((page - 1) * pageSize)
                .Limit(pageSize)
                .ToListAsync();

            return data;
        }

        public async Task<long> GetTotalArticles()
        {
            return await articlesCollection.EstimatedDocumentCountAsync();
        }

        public Task<long> GetTotalArticlesByType(string type)
        {
            var filter = Builders<Article>.Filter.Eq(x => x.ArticleType, type);
            var total = articlesCollection.CountDocumentsAsync(filter);

            return total;
        }

        public async Task<Article> SaveArticle(Article article)
        {
            await articlesCollection.InsertOneAsync(article);

            return article;
        }

        public async Task<Article> UpdateArticleAsync(Article article)
        {
            var filter = Builders<Article>.Filter.Eq(x => x.Id, article.Id);
            var result = await articlesCollection.ReplaceOneAsync(filter, article);
            return article;
        }
    }
}
